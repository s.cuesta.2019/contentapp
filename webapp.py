import socket

class WebApp:
    def parse(self, request):
        print("Parse: Not parsing anything")
        return None

    def process(self, parsedRequest):
        print("Process: Returning 200 OK")
        return "200 OK", "<html><body><h1>It works!</h1></body></html>"

    def __init__(self, hostname, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        mySocket.listen(5)

        while True:
            print(f"Waiting for connections (port: {port})")
            recvSocket, address = mySocket.accept()
            print("HTTP request received (going to parse and process):")
            request = recvSocket.recv(8 * 1024)
            print(request)
            parsedRequest = self.parse(request.decode('utf8'))
            returnCode, htmlAnswer = self.process(parsedRequest)
            print("Answering back...")
            response = f"HTTP/1.1 {returnCode}\r\n\r\n{htmlAnswer}\r\n"
            recvSocket.send(response.encode('utf8'))
            recvSocket.close()

if __name__ == "__main__":
    app = WebApp("localhost", 1234)