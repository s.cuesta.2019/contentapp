import webapp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

class ContentApp(webapp.WebApp):

    CONTENTS = {
        '/': "<p>Estas en la pagina principal</p>",
        '/hola': "<p>Hola a todos!</p>",
        '/contact': "<p>Contact us at: s.cuesta.2019@alumnos.urjc.es</p>",
        '/adios': "<p>Hasta la proxima!</p>"
    }

    def parse(self, request):
        return request.split(' ', 2)[1]

    def process(self, resource):
        content = self.CONTENTS.get(resource, None)
        code = "200 OK" if content else "404 Not Found"
        page = PAGE.format(content=content) if content else PAGE_NOT_FOUND.format(resource=resource)
        return code, page

if __name__ == "__main__":
    app = ContentApp("localhost", 1234)